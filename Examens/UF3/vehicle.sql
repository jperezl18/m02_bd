--Jorge Daniel Pérez López
--a232508jp

CREATE VIEW vehicle AS
SELECT matricula, 
       marca, 
       model, 
       'cotxe' AS tipus,
       CASE 
            WHEN tipus = 'Elèctric de bateria' THEN '0 emissions'
            WHEN tipus = 'Híbrid' THEN '0 emissions'
            WHEN subtipus = 'no endollable' THEN 'Eco'
            WHEN subtipus = 'gas liquat' THEN 'Eco'
            WHEN tipus = 'Propulsió gas' THEN 'Eco'
            WHEN subtipus = 'benzina' AND dataMatriculacio > '2006-01-31' THEN 'C'
            WHEN subtipus = 'diesel' AND dataMatriculacio > '2015-09-30' THEN 'C'
            WHEN subtipus = 'benzina' AND dataMatriculacio > '2001-01-31' THEN 'B'
            WHEN subtipus = 'diesel' AND dataMatriculacio > '2006-01-01' THEN 'B'
            ELSE 'Sense etiqueta'
        END "Etiqueta"
FROM cotxe

UNION

SELECT matricula,
       marca,
       model,
       'moto' AS tipus,
       'Sense classificar' AS "Etiqueta"
FROM moto;