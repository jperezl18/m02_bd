--() { :; }; exec psql template1 -f "$0"<
DROP DATABASE IF EXISTS  lloguerCotxes;
CREATE DATABASE lloguerCotxes;

\c lloguercotxes

/*
Creeu la vista vehicle on sortiran tots els vehicles de la BD.
Es demanen els següents camps:
matricula, marca,  model, tipus (serà cotxe o moto) i etiqueta,
tenint en compte que l'etiqueta serà una o altra depenent del següent:
* 0 emissions, quan el cotxe sigui  Elèctric de bateria ó  Híbrid endollable
* Eco, quan el cotxe sigui Híbrids no endollable, propulsió gas natural, propulsió gas liquat
* C, quan sigui benzina fins gener 2006 ó diesel fins setembre 2015
* B, quan sigui de benzina fins el gener 2001 ó diesel fins 2006

En el cas de les motos, a etiqueta haurà de sortir "Sense classificar"
*/
CREATE TABLE moto(
	matricula char(7) CONSTRAINT moto_matricula_pk PRIMARY KEY,
	marca varchar(20) CONSTRAINT moto_marca_nn NOT NULL,
	model varchar(40) CONSTRAINT moto_model_nn NOT NULL,
	color varchar(20) CONSTRAINT moto_color_nn NOT NULL,
	preu numeric(7,2),
	ofinum smallint,
	tipus varchar(50),
	subtipus varchar(50),
  dataMatriculacio date
);

insert into moto values
	('0000ABC','Harley-Davidson','LiveWire','Negra',45,1,'Elèctric de bateria','-','2020-02-12');

CREATE TABLE cotxe (
  matricula varchar(10) primary key,
  marca varchar(20),
  model varchar(20),
  color  varchar(20),
  codgrup varchar(1),
  ofinum smallint,
  diposit numeric(5,2),
  tipus varchar(50),
  subtipus varchar(50),
  dataMatriculacio date
);

insert into cotxe values
	('3333GHI','OPEL','ASTRA 2.0 DTI','Blau fosc','A',1,45,'Combustió interna','benzina','2006-10-23'),
	('1111ABC','FORD','FOCUS','Negre','A',1,40,'Combustió interna','diesel','2015-04-21'),
	('2222DEF','TOYOTA','PRIUS','Verd','A',2,52,'Híbrid','no endollable','2020-01-09'),
	('8888WXY','TESLA','Model 3','Verd','A',2,52,'Elèctric de bateria','-','2020-01-09'),
	('4444JKL','BMW','320 DTL','Gris','B',1,50,'Combustió interna','benzina','2002-06-06'),
	('5555MNO','VOLKSWAGEN','PASSAT VARIANT','Gris','B',2,53,'Combustió interna','diesel','2008-05-11'),
	('6666PQR','SMART','EQ FORTWO CABRIO','Negre obsidiana','A',2,null,'Propulsió gas','gas liquat','2019-03-02'),
	('7777STU','FORD','FIESTA','Gris','A',2,null,'Combustió interna','benzina','1998-03-07');