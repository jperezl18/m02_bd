#! /bin/bash
# Jorge D. Pérez López
# 22-03-2024
#
# Sinopsis: backupBD.sh user
# Descripció: Rep un usuari com a argument i crea backups de les
# BDs d'aquell usuari.
# ----------------------------------------------------------------

ERR_NARGS=1

if [ $# -ne 1 ] ; then
  echo "Error: El programa només requereix un argument."
  echo "Usage: $0 user"
  exit $ERR_NARGS
fi

user=$1

for bd in $(psql template1 -t -c "SELECT datname \
	                                FROM pg_database d \
			                            JOIN pg_roles r ON d.datdba=r.oid \
			                            WHERE rolname = '$user'")
do
  pg_dump -U $user -d $bd -C -f ${user}_${bd}_$(date +"%Y%m%d").sql
done

exit 0
