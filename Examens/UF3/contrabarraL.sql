--Jorge Daniel Pérez López
--a232508jp

SELECT datname "Name",
       rolname "Owner",
       CASE encoding
            WHEN 6 THEN 'UTF8'
       END "Encoding",
       datcollate "Collate",
       datctype "Ctype",
       daticulocale "ICU Locale",
       CASE datlocprovider
            WHEN 'c' THEN 'libc'
       END "locale provider",
       datacl "Access privileges"
FROM pg_database d
    JOIN pg_roles r ON d.datdba = r.oid
ORDER BY 1, 2;