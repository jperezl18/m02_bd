--Jorge Daniel Pérez
--a232508jp

/* 1. Mostra el llistat de pel·lícules les quals el gènere és drama y el director és
de nacionalitat anglesa o estatudinenca. Ha d'apareixer el títol, nom del director,
nom del gènere i nacionalitat. Ordena les pelis de més actual a més antiga. */
SELECT titol, nom director, genere, nacionalitat 
FROM pelicula p
JOIN director d ON p.coddir = d.coddir
JOIN genere g ON p.codgen=g.codgen
WHERE LOWER(nacionalitat) IN ('anglesa', 'estatunidenca') AND LOWER(genere)='drama';

-- 2.
SELECT p.codpeli, titol, d.coddvd, datapres, COALESCE(datadev::TEXT, 'En prèstec') "data devolucio"
FROM lloguer l
RIGHT JOIN dvd d ON l.coddvd=d.coddvd
JOIN pelicula p ON d.codpeli=p.codpeli 
WHERE TO_CHAR(datapres, 'DDMMYYYY') BETWEEN '01-02-2024' AND '29-02-2024';

-- 3.
SELECT cognoms||','||' '||nom soci, d.coddvd, p.codpeli, titol
FROM lloguer l
RIGHT JOIN soci s ON l.codsoci=s.codsoci
JOIN dvd d ON l.coddvd=d.coddvd
JOIN pelicula p ON d.codpeli=p.codpeli
ORDER BY 1;

-- 4.
SELECT codpeli, titol, ROUND((preu*0.79),2) "Preu sense IVA(€)", ROUND((preu*0.21),2) "IVA(€)", preu
FROM pelicula
WHERE preu BETWEEN 1.5 AND 3;