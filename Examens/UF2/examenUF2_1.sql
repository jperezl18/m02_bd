--Jorge Daniel Pérez López
--a232508jp

\c template1
DROP DATABASE IF EXISTS clubnautic;
CREATE DATABASE clubnautic;
\c clubnautic

CREATE SEQUENCE numSoci_seq
    START WITH 1
    INCREMENT BY 1;

CREATE TABLE Usuari (
    DNI VARCHAR(9) CONSTRAINT Usuari_DNI_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT Usuari_nom_nn NOT NULL,
    telefon VARCHAR(15) CONSTRAINT Usuari_telefon_nn NOT NULL,
    email VARCHAR(100),
    tipus VARCHAR(1) CONSTRAINT Usuari_tipus_ck CHECK (tipus = 'A' OR tipus = 'B' OR tipus = 'C'),
    dataAlta DATE DEFAULT CURRENT_DATE,
    numSoci INT CONSTRAINT Usuari_numSoci_nn NOT NULL,
    ciutat VARCHAR(100) CONSTRAINT Usuari_ciutat_nn NOT NULL
);

CREATE TABLE Vaixell (
    matricula VARCHAR(20) CONSTRAINT Vaixell_matricula_pk PRIMARY KEY,
    soci VARCHAR(9),
    nomVaixell VARCHAR(100),
    numAmarre INT CONSTRAINT Vaixell_numAmarre_nn NOT NULL,
    quota INT CONSTRAINT Vaixell_quota_nn NOT NULL,
    CONSTRAINT Vaixell_soci_fk FOREIGN KEY (soci) REFERENCES Usuari (DNI)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE Sortida (
    dataSortida TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    vaixell VARCHAR(20),
    patro VARCHAR(9),
    dataRetorn TIMESTAMP CONSTRAINT Sortida_dataRetorn_nn NOT NULL,
    destinacio VARCHAR(100) CONSTRAINT Sortida_destinacio_nn NOT NULL,
    CONSTRAINT Sortida_pk PRIMARY KEY (dataSortida,vaixell),
    CONSTRAINT Sortida_vaixell_fk FOREIGN KEY (vaixell) REFERENCES Vaixell (matricula)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT Sortida_patro_fk FOREIGN KEY (patro) REFERENCES Usuari (DNI)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT Sortida_dataSortida_ck CHECK (dataSortida < dataRetorn)
);

INSERT INTO Usuari (DNI, nom, telefon, email, tipus, dataAlta, numSoci, ciutat)
VALUES ('O4031927', 'Jorge Pérez', '+376682744', 'jperezl.andorra@gmail.com', 'A', '18/11/2015', NEXTVAL('numSoci_seq'), 'Barcelona'),
('12345678Z', 'Pedro Sanchez', 983123124, 'psanzhezc@moncloa.es', 'B', DEFAULT, NEXTVAL('numSoci_seq'), 'Madrid'),
('87654321H', 'Jude Bellingham', 389127419, null, 'C', DEFAULT, NEXTVAL('numSoci_seq'), 'Barcelona');

INSERT INTO Vaixell (matricula, soci, nomVaixell, numAmarre, quota)
VALUES ('ABC123', 'O4031927', 'Southern Wind', 1, 1237),
('DEF456', '12345678Z', 'Summer Bay', 2, 345),
('GHI789', 'O4031927', null, 3, 124),
('JKL012', '87654321H', 'Northwestern', 4, 198469);

INSERT INTO Sortida (dataSortida, vaixell, patro, dataRetorn, destinacio)
VALUES (DEFAULT, 'JKL012', '87654321H', '26/11/2025', 'Londres');

DELETE FROM Usuari
WHERE ciutat='Barcelona';

UPDATE Vaixell
SET quota = quota * 1.2
WHERE quota < 400;