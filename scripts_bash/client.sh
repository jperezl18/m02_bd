#! /bin/bash
# Jorge D. Pérez López
# 15/03/2024
#
# Sinopsis: client.sh repcod
#
# Descripció: El programa rep un codi de representant i mostra
# els clients d'aquell representant.
#-------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 1 ] ; then
  echo "Error: El programa ha de tenir només un argument."
  echo "Usage: $0 repcod"
  exit $ERR_NARGS
fi

repcod=$1

IFS=$'\n'

for client in $(psql training -t -c "SELECT nombre FROM cliente WHERE repcod = $repcod;")
do
  echo $client 
done

exit 0
