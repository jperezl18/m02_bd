#! /bin/bash
# Jorge D. Pérez López
# 16/03/2024
#
# Sinopsis: deleteExpiredUsers.sh
#
# Descripció: El programa esborra usuaris caducats del servidor psql.
#----------------------------------------------------------------------

ERR_NARGS=1

if [ $# -ne 0 ] ; then
  echo "Error: El programa no requereix arguments"
  echo "Usage: $0"
  exit $ERR_NARGS
fi

usuaris_borrats=0

for user in $(psql template1 -t -c "SELECT rolname \
                                    FROM pg_roles \ 
                                    WHERE rolvaliduntil < CURRENT_TIMESTAMP;")
do
  psql template1 -c "DROP ROLE $user;"
  if [ $? -eq 0 ] ; then
    echo "Usuari $user eliminat."
  fi
  ((usuaris_borrats++))
done

echo "$usuaris_borrats Usuaris eliminats."

exit 0
