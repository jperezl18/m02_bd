#! /bin/bash
# Jorge D. Pérez López
# 15/03/2024
#
# Sinopsis: newUser.sh user password expiration_date (YYYY-MM-DD)
#
# Descripció: El programa rep un nom d'usuari, una contrasenya i 
# la data de venciment del mateix i el dona d'alta al servidor de base de dades.
#---------------------------------------------------------------------------------

ERR_NARGS=1
ERR_DATE=2
ERR_CREATE=3

if [ $# -ne 3 ] ; then
  echo "Error: El programa ha de tenir 3 arguments."
  echo "Usage: $0 user password expiration_date (YYYY-MM-DD)"
  exit $ERR_NARGS
fi

echo $3 | grep -q -E "^[0-9]{4}-[0-9]{2}-[0-9]{2}$"

if [ $? -ne 0 ] ; then
  echo "Error: Data no vàlida."
  echo "Format: YYYY-MM-DD"
  exit $ERR_DATE
else
  user=$1
  password=$2
  exp_date=$3
fi

psql template1 -c "CREATE ROLE $user \
                   LOGIN \ 
                   CREATEDB \
                   PASSWORD '$password' \
                   VALID UNTIL '$exp_date';"

if [ $? -ne 0 ] ; then
  echo "Error: no s'ha progut crear l'usuari $user."
  exit $ERR_CREATE
fi

psql template1 -c "SELECT oid, \ 
                          rolname, \ 
                          rolcreatedb, 
                          rolcanlogin, \ 
                          rolvaliduntil \ 
                          FROM pg_roles \ 
                          WHERE rolname = '$user';"


exit 0
