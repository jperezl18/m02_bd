/* Nom representant, ciutat oficina, rang de vendes considerant el rendiment de vendes. */

SELECT nombre, 
	COALESCE(ciudad, '----') ciudad,
	CASE
		WHEN (r.ventas*100/r.cuota) BETWEEN 0 AND 50 THEN 
			'Rendiment baix'
		WHEN (r.ventas*100/r.cuota) BETWEEN 50.00000000000001 AND 75 THEN
			'Rendiment mitjà'
		WHEN (r.ventas*100/r.cuota) BETWEEN 75.00000000000001 AND 100 THEN
			'Rendiment alt'
		ELSE
			'Rendiment excel·lent'
	END rendiment
FROM repventa r
	LEFT JOIN oficina o ON r.ofinum=o.ofinum;

/* Mostra el nom dels objectes de la base de dades (\d)*/
SELECT nspname "Esquema",
       relname "Nom", 
        CASE
            WHEN relkind='i' THEN
                    'Index'
            WHEN relkind='r' THEN
                    'Taula'
            WHEN relkind='v' THEN
                    'Vista'
            WHEN relkind='S' THEN
                    'Seqüencia'
            ELSE
                    'Altres'
        END "Tipus",
        rolname "Propietari" 
FROM pg_class c
    JOIN pg_roles r ON c.relowner=r.oid
    JOIN pg_namespace n ON c.relnamespace=n.oid
WHERE relkind IN ('r', 'v', 'S') AND LOWER(rolname) = 'jperezl'
ORDER BY 1, 2;
