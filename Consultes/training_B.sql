-- Jorge Daniel Pérez López
-- a232508jp

\c training

/* 1. Muestra de los representantes su nombre, la ciudad de su oficina así como su región. */
SELECT nombre, ciudad, region 
FROM repventa
JOIN oficina ON repventa.ofinum=oficina.ofinum;

/* 2. Obtener una lista de todos los pedidos, mostrando el número de pedido, su importe, el nombre del cliente que lo realizó 
y el límite de crédito de dicho cliente. */
SELECT pednum, importe, nombre AS "cliente", limcred 
FROM pedido p
JOIN cliente c ON p.cliecod=c.cliecod;

/* 3. Obtener una lista de representantes ordenada alfabéticamente, en la que se muestre el nombre del representante, codigo de la
oficina donde trabaja, ciudad y la región a la que vende. */
SELECT nombre AS "representante", o.ofinum, ciudad, region 
FROM repventa r
JOIN oficina o ON r.ofinum=o.ofinum
ORDER BY nombre;

/* 4. Obtener una lista de las oficinas (ciudades, no códigos) que tienen un objetivo superior a 360000 euros. 
Para cada oficina mostrar la ciudad, su objetivo, el nombre de su director y puesto del mismo. */
SELECT ciudad, objetivo, nombre AS "director", puesto 
FROM oficina
JOIN repventa ON director=repcod
WHERE objetivo > 360000;

/* 5. Obtener una lista de todos los pedidos mostrando su número, el importe y la descripción de los productos solicitados. */
SELECT pednum, importe, descrip 
FROM pedido pe
JOIN producto pr ON pe.fabcod||pe.prodcod=pr.fabcod||pr.prodcod;

/* 6. Obtener una lista de los pedidos con importes superiores a 4000. Mostrar el nombre del cliente que solicitó el pedido,
numero del pedido, importe del mismo, la descripción del producto solicitado y el nombre del representante que lo tomó. Ordenad la
lista por cliente alfabéticamente y luego por importe de mayor a menor. */
SELECT c.nombre AS "cliente", pednum, importe, descrip, r.nombre AS "representante" 
FROM pedido pe
JOIN cliente c ON pe.cliecod=c.cliecod
JOIN repventa r ON pe.repcod=r.repcod
JOIN producto pr ON pe.prodcod=pr.prodcod
WHERE importe > 4000
ORDER BY c.nombre, importe DESC;

/* 7. Obtener una lista de los pedidos con importes superiores a 2000 euros, mostrando el número de pedido, importe, nombre del
cliente que lo solicitó y el nombre del representante que contactó con el cliente por primera vez. */
SELECT pednum, importe, c.nombre cliente, r.nombre representante 
FROM pedido p
JOIN cliente c ON p.cliecod=c.cliecod
JOIN repventa r ON c.repcod=r.repcod
WHERE importe > 2000;

/* 8. Obtener una lista de los pedidos con importes superiores a 150 euros, mostrando el código del pedido, el importe, el nombre del
cliente que lo solicitó, el nombre del representante que contactó con él por primera vez y la ciudad de la oficina donde el
representante trabaja. */
SELECT pednum, importe, c.nombre cliente, r.nombre representante, ciudad 
FROM pedido p
JOIN cliente c ON p.cliecod=c.cliecod
JOIN repventa r ON c.repcod=r.repcod
JOIN oficina o ON r.ofinum=o.ofinum
WHERE importe > 150;

/* 9. Lista los pedidos tomados durante el mes de octubre del año 2003, mostrando solamente el número del pedido, su importe, el nombre del
cliente que lo realizó, la fecha y la descripción del producto solicitado */
SELECT pednum, importe, c.nombre cliente, fecha, descrip 
FROM pedido pe
JOIN cliente c ON pe.cliecod=c.cliecod
JOIN producto pr ON pe.prodcod=pr.prodcod
WHERE fecha BETWEEN '2003-10-01' AND '2003-10-31';

/* 10. Obtener una lista de todos los pedidos tomados por representantes de oficinas de la región Este, mostrando solamente el número del
pedido, la descripción del producto y el nombre del representante que lo tomó */
SELECT pednum, descrip, nombre representante 
FROM pedido pe
JOIN producto pr ON pe.prodcod=pr.prodcod
JOIN repventa r ON pe.repcod=r.repcod
JOIN oficina o ON r.ofinum=o.ofinum
WHERE region='Este';

/* 11. Obtener los pedidos tomados en los mismos días en que un nuevo representante fue contratado. Mostrar número de pedido, importe,
fecha pedido. */
SELECT pednum, importe, fecha 
FROM pedido p
JOIN repventa r ON p.fecha=r.fcontrato;

/* 12. Obtener una lista con parejas de representantes y oficinas en donde la cuota del representante es mayor o igual que el objetivo de la
oficina, sea o no la oficina en la que trabaja. Mostrar Nombre del representante, cuota del mismo, Ciudad de la oficina, objetivo de la misma */

/* 13. Muestra el nombre, las ventas y la ciudad de la oficina de cada representante de la empresa. */ 
SELECT nombre, r.ventas, ciudad 
FROM repventa r
LEFT JOIN oficina o ON r.ofinum=o.ofinum;

/* 14. Obtener una lista de la descripción de los productos para los que existe algún pedido en el que se solicita una cantidad mayor a las
existencias de dicho producto. */
SELECT DISTINCT descrip 
FROM producto pr
JOIN pedido pe ON (pr.fabcod,pr.prodcod)=(pe.fabcod,pe.prodcod)
WHERE exist < cant;

/* 15. Lista los nombres de los representantes que tienen una cuota superior a la de su director. */
SELECT r.nombre 
FROM repventa r
JOIN oficina o ON r.ofinum=o.ofinum
JOIN repventa director ON director.repcod=o.director
WHERE r.cuota > director.cuota;

/* 16. Obtener una lista de los representantes que trabajan en una oficina distinta de la oficina en la que trabaja su director, mostrando
también el nombre del director y el código de la oficina donde trabaja cada uno de ellos. */
SELECT r.nombre representante, r.ofinum "oficina representante", director.nombre director, director.ofinum "oficina director" 
FROM repventa r
JOIN oficina o ON r.ofinum=o.ofinum
JOIN repventa director ON director.repcod=o.director
WHERE r.ofinum != director.ofinum;

/* 17. El mismo ejercicio anterior, pero en lugar de ofinum, la ciudad. */
SELECT r.nombre representante, o.ciudad "ciudad representante", director.nombre director, OfDir.ciudad "ciudad director"
FROM repventa r
JOIN oficina o ON r.ofinum=o.ofinum
JOIN repventa director ON director.repcod=o.director
JOIN oficina OfDir ON director.ofinum=OfDir.ofinum
WHERE r.ofinum != director.ofinum;

/* 18. Mostrar el nombre y el puesto de los que son jefe. */
SELECT DISTINCT j.nombre, j.puesto 
FROM repventa r
JOIN repventa j ON r.jefe=j.repcod;

/* Mostrar el nom de cada representant i el nom del seu cap. Si no té cap, ha de sortir ---- */
SELECT r.nombre, COALESCE(j.nombre,'----') jefe FROM repventa r
LEFT OUTER JOIN repventa j ON r.jefe=j.repcod;