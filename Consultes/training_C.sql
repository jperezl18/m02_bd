-- Jorge Daniel Pérez López
-- a232508jp

\c training

/* 1. Mostrar la suma de las cuotas y la suma de las ventas totales de
todos los representantes. */
SELECT SUM(cuota) "Total cuotas", SUM(VENTAS) "Total ventas" 
FROM repventa;

/* 2. ¿Cuál es el importe total de los pedidos tomados por Bill Adams? */
SELECT SUM(importe) "Pedidos Bill"
FROM pedido p
JOIN repventa r ON p.repcod=r.repcod
WHERE LOWER(nombre)='bill adams';

/* 3. Calcula el precio medio de los productos del fabricante ACI. */
SELECT ROUND(AVG(precio),2) "precio medio"
FROM producto
WHERE fabcod='aci';

/* 4. ¿Cuál es el importe medio de los pedido solicitados por el cliente
"acme mfg." */
SELECT ROUND(AVG(importe),2) "Importe medio"
FROM pedido p
JOIN cliente c ON p.cliecod=c.cliecod
WHERE LOWER(nombre)='acme mfg.';

/* 5. Mostrar la cuota máxima y la cuota mínima de las cuotas de los
representantes. */
SELECT MAX(cuota) "Cuota máxima", MIN(cuota) "Cuota mínima"
FROM repventa;

/* 6. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado? */
SELECT MIN(fecha) "Primer pedido"
FROM pedido;

/* 7. ¿Cuál es el mejor rendimiento de ventas de todos los representantes?
(considerarlo como el porcentaje de ventas sobre la cuota). */
SELECT ROUND(MAX(ventas*100/cuota),2)::TEXT || '%' AS "Mayor rendimiento %"
FROM repventa;

/* 8. ¿Cuántos clientes tiene la empresa? */
SELECT COUNT(*) "Núm. clientes"
FROM cliente;

/* 9. ¿Cuántos representantes han obtenido un importe de ventas superior a
su propia cuota? */
SELECT COUNT(*) "Representantes que han superado su cuota"
FROM repventa 
WHERE ventas > cuota;

/* 10. ¿Cuántos pedidos se han tomado de más de 150 euros? */
SELECT COUNT(*) "N. de pedidos"
FROM pedido
WHERE importe > 150;

/* 11. Halla el número total de pedidos, el importe medio, el importe total
de los mismos. */
SELECT COUNT(*) "Total de pedidos", ROUND(AVG(importe),2) "Media de importe", SUM(importe) "Importe total"
FROM pedido;

/* 12. ¿Cuántos puestos de trabajo diferentes hay en la empresa? */
SELECT COUNT(DISTINCT puesto) "N. de Puestos"
FROM repventa;

/* 13. ¿Cuántas oficinas de ventas tienen representantes que superan sus
propias cuotas? */
SELECT COUNT(DISTINCT ofinum) "N. oficinas"
FROM repventa
WHERE ventas > cuota;

/* 14. ¿Cuál es el importe medio de los pedidos tomados por cada
representante? */
SELECT nombre, ROUND(AVG(importe),2) "Importe medio"
FROM pedido p
JOIN repventa r ON p.repcod=r.repcod
GROUP BY r.repcod;

/* 15. ¿Cuál es el rango de las cuotas de los representantes asignados a
cada oficina (mínimo y máximo)? */
SELECT ofinum, MAX(cuota) "Cuota màxima", MIN(cuota) "Cuota mínima"
FROM repventa
WHERE ofinum IS NOT NULL
GROUP BY ofinum;

/* 16. ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad
y número de representantes. */
SELECT ciudad, COUNT(repcod) "N. de representantes"
FROM repventa r
JOIN oficina o ON r.ofinum=o.ofinum
GROUP BY ciudad;

/* 17. ¿Cuántos clientes ha contactado por primera vez cada representante?
Mostrar el código de representante, nombre y número de clientes. */
SELECT r.nombre, r.repcod representante, COUNT(cliecod) "N. de clientes"
FROM cliente c
JOIN repventa r ON c.repcod=r.repcod
GROUP BY r.repcod;

/* 18. Calcula el total del importe de los pedidos solicitados por cada
cliente a cada representante. */
SELECT SUM(importe) total, r.nombre representante, c.nombre cliente
FROM pedido p
JOIN cliente c ON p.cliecod=c.cliecod
JOIN repventa r ON p.repcod=r.repcod
GROUP BY r.repcod, c.cliecod;

/* 19. Lista el importe total de los pedidos tomados por cada
representante. */
SELECT SUM(importe) "Total pedidos", nombre representante
FROM pedido p
JOIN repventa r ON p.repcod=r.repcod
GROUP BY r.repcod;

/* 20. Para cada oficina con dos o más representantes, calcular el total de
las cuotas y el total de las ventas de todos sus representantes. */
SELECT o.ofinum, SUM(cuota) cuota, SUM(r.ventas) ventas
FROM repventa r
JOIN oficina o ON r.ofinum=o.ofinum
GROUP BY o.ofinum
HAVING COUNT(r.repcod) >= 2;

/* 21. Muestra el número de pedidos que superan el 75% de las existencias. */
SELECT COUNT(*) "N. de pedidos exist > 75%"
FROM pedido pe
JOIN producto pr ON pe.fabcod||pe.prodcod=pr.fabcod||pr.prodcod
WHERE cant > exist*0.75;

-- Taula temporal:
SELECT MAX(mitjana)
FROM (SELECT AVG(sal) mitjana
	  FROM emp
	  GROUP BY deptno) mitjana_emp;