-- Jorge Daniel Pérez López
-- a232508jp

/* 0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho
con self join, ahora con subconsultas) */
SELECT nombre, puesto
FROM repventa
WHERE repcod IN (SELECT jefe
                 FROM repventa);

/* 1. Obtener una lista de los representantes cuyas cuotas son iguales ó
superiores al objetivo de la oficina de Atlanta. */
SELECT *
FROM repventa
WHERE cuota >= (SELECT objetivo
                FROM oficina
                WHERE LOWER(ciudad) = 'atlanta');

/* 2. Obtener una lista de todos los clientes (nombre) que fueron
contactados por primera vez por Bill Adams. */
SELECT nombre
FROM cliente
WHERE repcod = (SELECT repcod
                FROM repventa
                WHERE LOWER(nombre) = 'bill adams');

/* 3. Obtener una lista de todos los productos del fabricante ACI cuyas
existencias superan a las existencias del producto 41004 del mismo
fabricante. */
SELECT *
FROM producto
WHERE LOWER(fabcod) = 'aci' AND exist > (SELECT exist
                                         FROM producto
                                         WHERE LOWER(fabcod) = 'aci' AND prodcod = '41004');

/* 4. Obtener una lista de los representantes que trabajan en las oficinas
que han logrado superar su objetivo de ventas. */
SELECT *
FROM repventa
WHERE ofinum IN (SELECT ofinum
                FROM oficina
                WHERE ventas > objetivo);

/* 5. Obtener una lista de los representantes que no trabajan en las
oficinas dirigidas por Larry Fitch. */
SELECT *
FROM repventa
WHERE ofinum NOT IN (SELECT o.ofinum
                     FROM oficina o 
                        JOIN repventa r ON director=repcod
                     WHERE LOWER(nombre) = 'larry fitch' AND director IS NOT NULL);

/* 6. Obtener una lista de todos los clientes que han solicitado pedidos
del fabricante ACI entre enero y junio de 2003. */
SELECT *
FROM cliente
WHERE cliecod IN (SELECT cliecod
                  FROM pedido
                  WHERE LOWER(fabcod) = 'aci' AND TO_CHAR(fecha, 'DD-MM-YYYY') BETWEEN '01-01-2003' AND '30-06-2003');

/* 7. Obtener una lista de los productos de los que se ha tomado un pedido
de 150 euros ó mas. */
SELECT *
FROM producto
WHERE (fabcod, prodcod) IN (SELECT fabcod, prodcod
                            FROM pedido
                            WHERE importe >= 150);

/* 8. Obtener una lista de los clientes contactados por Sue Smith que
no han solicitado pedidos con importes superiores a 18 euros. */
SELECT nombre, repcod
FROM cliente
WHERE repcod IN (SELECT repcod
                 FROM repventa
                 WHERE LOWER(nombre)= 'sue smith' AND cliecod NOT IN (SELECT cliecod
                                                                      FROM pedido
                                                                      WHERE importe > 18));

/* 9. Obtener una lista de las oficinas en donde haya algún
representante cuya cuota sea más del 55% del objetivo de la oficina.
Para comprobar vuestro ejercicio, haced una Consulta previa cuyo
resultado valide el ejercicio. */
SELECT *
FROM oficina
WHERE ofinum IN (SELECT r.ofinum
                 FROM oficina o 
                    JOIN repventa r ON r.ofinum=o.ofinum
                 WHERE cuota > objetivo*0.55);

/* 10. Obtener una lista de los representantes que han tomado algún
pedido cuyo importe sea más del 10% de de su cuota. */
SELECT *
FROM repventa
WHERE repcod IN (SELECT repcod
                 FROM pedido
                 WHERE importe > 0.1 * cuota);
      
/* 11. Obtener una lista de las oficinas en las cuales el total de
ventas de sus representantes han alcanzado un importe de ventas
que supera el 50% del objetivo de la oficina. Mostrar también el
objetivo de cada oficina (suponed que el campo ventas de oficina no
existe). */
SELECT ofinum, objetivo
FROM oficina o
WHERE (SELECT SUM(ventas)
       FROM repventa r
       WHERE r.ofinum=o.ofinum) > 0.5 * objetivo;

/* 12. ¿Cuál es la descripción del primer producto solicitado en un
pedido? */
SELECT descrip
FROM producto
WHERE (fabcod, prodcod) IN (SELECT fabcod, prodcod
                            FROM pedido
                            ORDER BY fecha DESC
                            LIMIT 1);

/* 13. ¿Qué representante tiene el mejor porcentaje de ventas? */
SELECT nombre "Nombre representante",
                (SELECT ROUND((ventas/cuota)*100 ,2 )FROM repventa WHERE repcod=r.repcod)"pordentaje_ventas"
FROM repventa r
ORDER BY pordentaje_ventas ASC
LIMIT 1;

/* 14. ¿Qué representante tiene el peor porcentaje de ventas? */
SELECT nombre "Nombre representante",
                (SELECT COALESCE(ROUND((ventas/cuota)*100 , 2 )::TEXT,'no te ventas')
                FROM repventa
                WHERE repcod=r.repcod)"pordentaje_ventas"
FROM repventa r
ORDER BY pordentaje_ventas DESC
LIMIT 1;

/* 15. ¿Qué producto (Descripción) tiene más pedidos? */

/* 16 . ¿Qué producto se ha vendido más? */

