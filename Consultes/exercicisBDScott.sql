--Jorge Daniel Pérez López
--a232508jp

/* 41. Calcular el salari total mensual. */
SELECT SUM(sal) "Salari total"
FROM emp;

/* 42.  Per a cada departament, mostreu el nom de departament, localitat, 
número d'empleats assignats i acumulat de salaris. */
SELECT dname, loc, COUNT(empno) "N. empleats", SUM(sal) "Salari total"
FROM emp e
    JOIN dept d ON e.deptno=d.deptno
GROUP BY dname, loc;

/* 43. Mostreu per cada departament (mostreu el nom) l'acumulat de salaris, 
l'acumulat de comissions i la suma de tots dos. */
SELECT dname, COALESCE(SUM(sal)::TEXT, 'No sal') "Salari total", 
    COALESCE(SUM(comm)::TEXT, 'No coms') "Comisions totals", 
    COALESCE(SUM(sal)+SUM(comm), SUM(sal)) "Salaris + coms"
FROM emp e
    RIGHT JOIN dept d ON e.deptno=d.deptno
GROUP BY dname;

/* 44. Inseriu un nou empleat sense assignar-li ocupació. A continuació 
mostreu el salari, mínim i màxim dels empleats, agrupats per ocupació. */
INSERT INTO emp (empno, ename, hiredate, sal, comm, deptno)
VALUES (7777, 'JORGE', TO_DATE('18-11-2003', 'DD-MM-YYYY'), 777, 70, 40);

SELECT MIN(sal) "Salari més baix", 
    MAX(SAL) "Salari més alt", 
    COALESCE(job, 'Sense assignar') job
FROM emp
GROUP BY COALESCE(job, 'Sense assignar');

/* 45. Mostreu el salari mínim, màxim i mig dels empleats agrupats per feina, 
però només d'aquells la mitjana sigui superior a 4000. */
SELECT MIN(sal) "Salari mínim", 
    MAX(sal) "Salari màxim", 
    ROUND(AVG(sal),2) "salari mig", 
    COALESCE(job, 'Sense assignar') "job"
FROM emp
GROUP BY job
HAVING AVG(sal) > 4000;

/* 46. Mostreu el codi i el nom dels departaments que tinguin més de 
tres empleats assignats. */
SELECT d.deptno, dname
FROM dept d
    JOIN emp e ON d.deptno=e.deptno
GROUP BY d.deptno, dname
HAVING COUNT(empno) > 3;

/* 54. Mostreu el treball, el nom i el salari dels empleats ordenats 
pel tipus de treball i per salari descendent. */
SELECT COALESCE(job, 'No assignat') job, ename, sal
FROM emp
ORDER BY job DESC, sal DESC;

/* 55. Mostreu el nom de cada empleat, i el nombre i nom del seu cap. */
SELECT e.ename, COALESCE(c.ename, '----') cap 
FROM emp e
LEFT JOIN emp c ON e.mgr=c.empno;

/* 56. Mostreu dels empleats que son analyst el nom de l'empleat, la seva 
data d'alta en l'empresa, anys, mesos i dies que porta contractat 
(no podeu utilitzar cap funcio) dels empleats que són analyst. */
SELECT ename, hiredate,
    EXTRACT(YEAR FROM age(CURRENT_DATE, hiredate)) Anys,
    EXTRACT(MONTH FROM age(CURRENT_DATE, hiredate)) Mesos,
    EXTRACT(DAY FROM age(CURRENT_DATE, hiredate)) Dies
FROM emp
WHERE LOWER(job) = 'analyst';

/* 57. Trobeu el salari mitjà d'aquells empleats el treball sigui 
el d'analista (analyst). */
SELECT ROUND(AVG(sal),2) "Salari mitjà"
FROM emp
WHERE LOWER(job) = 'analyst';

/* 58. Trobeu el salari més alt, el més baix i la diferència 
entre tots dos. */
SELECT MAX(sal) "Salari més alt", MIN(sal) "Salari més baix", MAX(sal)-MIN(sal) diferència
FROM emp;

/* 59. Trobeu el nombre de treballs diferents que hi ha al 
departament 30. */
SELECT COUNT(DISTINCT job) "N. de treballs"
FROM emp
WHERE deptno = 30;

/* 60. Mostreu el nom de l'empleat, el seu treball, el nom 
i el codi de l'departament en el qual treballa. */
SELECT ename, COALESCE(job, 'No assignat') job, dname, e.deptno
FROM emp e
    JOIN dept d ON e.deptno=d.deptno;

/* 62. Trobeu als empleats el cap del qual és 'BLAKE'. */
SELECT e.ename
FROM emp e
    LEFT JOIN emp c ON e.mgr=c.empno
WHERE LOWER(c.ename) = 'blake';

/* 63. Trobeu el nombre de treballadors diferents en el 
departament 30 per a aquells empleats el salari pertanyi 
a l'interval [1000, 1800]. */
SELECT ename
FROM emp
WHERE sal BETWEEN 1000 AND 1800 AND deptno = 30;

/* 65. El salari mitjà i mínim de cada lloc, mostrant en el 
resultat aquells el salari mitjà estigui per sobre de 1500. */
SELECT ROUND(AVG(sal),2) "Salari mig", MIN(sal) "Salari més baix", job
FROM emp
GROUP BY job
HAVING AVG(sal) > 1500;

/* 66. Què empleats treballen a 'DALLAS'? */
SELECT ename
FROM emp e
    JOIN dept d ON e.deptno=d.deptno
WHERE LOWER(loc) = 'dallas';

/* 67. Quants empleats treballen a 'CHICAGO'? */
SELECT COUNT(*) "N. de empleats"
FROM emp e
    JOIN dept d ON e.deptno=d.deptno
WHERE LOWER(loc) = 'chicago';

/* 68. Llistar el nom dels empleats que guanyen menys que els seus caps */
SELECT e.ename
FROM emp e
    LEFT JOIN emp c ON e.mgr=c.empno
WHERE e.sal < c.sal;

/* 69. Llistar el nom dels empleats que guanyen més que els seus caps */
SELECT e.ename
FROM emp e
    LEFT JOIN emp c ON e.mgr=c.empno
WHERE e.sal > c.sal;

/* 70. Llistar el nom, treball, departament, localitat i salari d'aquells empleats
 que tinguin un salari major de 2000 i treballin a 'DALLAS' o 'NEW YORK'. */
SELECT ename, job, e.deptno, loc, sal
FROM emp e
    JOIN dept d ON e.deptno=d.deptno
WHERE LOWER(loc) IN ('dallas', 'new york') AND sal > 2000;

/* 71. Mostreu els empleats que treballen en el mateix departament que Clark. */
SELECT e1.ename
FROM emp e1
    JOIN emp e2 ON e1.deptno = e2.deptno
WHERE LOWER(e2.ename) = 'clark' AND e1.empno != e2.empno;

/* 72. Indiqueu (sense executar el codi) si les següents sentències són correctes. 
En cas contrari indiqueu en què consisteix l'error: */

--A
SELECT *  
FROM EMP  
WHERE MGR IS NULL; 

/* No es pot igualar a null, s'ha d'indicar que el camp sigui null. */

--B
SELECT *  
FROM DEPT  
WHERE DEPTNO = 20 OR DEPTNO = 30;

/* Només ha d'haver un WHERE. */

--C
SELECT *  
FROM EMP  
WHERE NOT ename LIKE 'R%' AND sal BETWEEN 3000 AND 5000;

/* La consulta és correcta. */

--D
SELECT *  
FROM EMP  
WHERE sal < 4000 AND NOT job = 'ANALYST';

/* El NOT va abans de la condició. */

--E
SELECT *  
FROM DEPT  
WHERE loc = 'DALLAS' OR loc = 'CHICAGO';

/* S'ha d'usar la clausula IN o si no s'ha d'especificar el camp en les
dues condicions. */