# 1. Museus

`Una persona amant de l'art vol construir una base de dades de museus del món i les obres d'art de cadascú. Per les limitacions del seu equip informàtic (i les seves pròpies) va considerar únicament pintures i escultures.
Per la informació de què disposa, pot saber en quin museu està cada obra d'art i, a més es coneix la sala del museu en la qual està l'obra. Les sales dels museus tenen un nom i pot haver sales amb mateix nom en diferents museus.
Com a aficionat a la matèria que és, sap que tota obra d'art té un títol.
Altres dades són específics del tipus d'obra d'art que consideri: pintura o escultura. Així, de les pintures es coneix el seu format de anchoxalto i el tipus de pintura (oli, pastel, aquarel·la, ...). De les escultures es considera el material amb què estan fetes (bronze, ferro, marbre, ...) i l'estil de l'escultura (neoclàssica, grecoromana, cubista, ...).
Li interessarà també, conèixer els autors de les obres. Les dades generals dels autors seran el seu nom i nacionalitat. Com és natural, hi ha obres d'art de les que es desconeix l'autor.
Dels museus recollirà la següent informació: el nom del museu, direcció amb el nom del carrer i el número, a més de la ciutat i el país on està.
Nota: establiu les claus primàries que considereu oportunes.`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas museus](https://drive.google.com/file/d/1g5Ij4QjA1NUHdBeLCd1mMvz1eDR6DHIL/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas museus](./5_museus.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  museu(<ins>id_museu</ins>, nom, carrer, numero, ciutat, pais)  
  sala(<ins>id_museu</ins>, <ins>id_sala</ins>, nom)  
  obra(<ins>id_obra</ins>, titol, data_creacio, tipus, format, tipus_pintura, material, estil_escultura, id_sala)  
  autor(<ins>id_autor</ins>, nom, nacionalitat)  
  obres_autors(<ins>id_obra</ins>, <ins>id_autor</ins>) 

## 3.2. Diagrama referencial
Relació referencial|Clau aliena|Relació referida
-|:-:|-
sala|id_museu|museu
obra|id_sala|sala
obres_autor|id_obra|obra
obres_autor|id_autor|autor

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 5_museus.sql](./5_museus.sql)
