-- Jorge Daniel Pérez López
-- a232508jp

\c template1
DROP DATABASE IF EXISTS museus;
CREATE DATABASE museus;
\c museus

CREATE TABLE museu (
    id_museu INT CONSTRAINT museu_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT museu_nom_nn NOT NULL,
    carrer VARCHAR(100) CONSTRAINT museu_carrer_nn NOT NULL,
    numero NUMERIC(4) CONSTRAINT museu_numero_nn NOT NULL,
    ciutat VARCHAR(100) CONSTRAINT museu_ciutat_nn NOT NULL,
    pais VARCHAR(100) CONSTRAINT museu_pais_nn NOT NULL
);

CREATE TABLE sala (
    id_museu INT,
    id_sala INT CONSTRAINT sala_id_sala_uk UNIQUE,
    nom VARCHAR(100),
    CONSTRAINT sala_pk PRIMARY KEY (id_museu,id_sala),
    CONSTRAINT sala_id_museu_fk FOREIGN KEY (id_museu) REFERENCES museu (id_museu)
);

CREATE TABLE obra (
    id_obra INT CONSTRAINT obra_id_pk PRIMARY KEY,
    titol VARCHAR(100),
    data_creacio DATE,
    tipus VARCHAR(15) CONSTRAINT obra_tipus_ck CHECK (tipus = 'Pintura' OR tipus = 'Escultura'),
    format VARCHAR(20),
    tipus_pintura VARCHAR(100),
    estil_escultura VARCHAR(100),
    material VARCHAR(100),
    id_sala INT,
    CONSTRAINT obra_id_sala_fk FOREIGN KEY (id_sala) REFERENCES sala (id_sala)
);

CREATE TABLE autor (
    id_autor INT CONSTRAINT autor_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT autor_nom_nn NOT NULL,
    nacionalitat VARCHAR(100)
);

CREATE TABLE obres_autors (
    id_autor INT,
    id_obra INT,
    CONSTRAINT obres_autors_pk PRIMARY KEY (id_autor,id_obra),
    CONSTRAINT obres_autors_id_autor_fk FOREIGN KEY (id_autor) REFERENCES autor (id_autor),
    CONSTRAINT obres_autors_id_obra_fk FOREIGN KEY (id_obra) REFERENCES obra (id_obra)
);