--Jorge Daniel Pérez López
--a232508jp

\c template1
DROP DATABASE IF EXISTS institut
CREATE DATABASE institut;
\c institut

CREATE TABLE pas (
    idPas INT CONSTRAINT pas_idPas_pk PRIMARY KEY,
    nom VARCHAR(100)
);

CREATE TABLE professor (
    idProfe INT CONSTRAIT professor_idProfe_pk PRIMARY KEY,
    nom VARCHAR(100),
    dept INT
);

INSERT INTO pas VALUES
(1, 'Julia'),
(2, 'Nil');

INSERT INTO professor VALUES
(1, 'Anowar', 100),
(2, 'Pedro Sanchez', 200);

CREATE VIEW personal AS
SELECT idPas codi, nom, 'pas' AS tipus
FROM pas

UNION ALL 

SELECT idProfe codi, nom, 'professor' AS tipus
FROM professor;