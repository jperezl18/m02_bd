# 1. Habitatges

## Habitatges
Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (requeriments d'usuari) complementaris necessaris.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas habitatges](https://drive.google.com/file/d/15SrNzN9V3LA_JImQ3PtC31WNpBdzxzSF/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas habitatges](./1_habitatges.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  municipi(<ins>id_municipi</ins>,nom, habitants, extensió)  
  habitatge(<ins>num_cadastre</ins>,carrer, num, pis, porta, escala, cp, m2, id_municipi)  
  persona(<ins>dni</ins>, nom, cognoms, num_cadastre, cap_familia)  
  propietat(<ins>dni</ins>,<ins>num_cadastre</ins>)
  \...

## 3.2. Diagrama referencial

* El diagrama referencial em serveix per indicar quines claus alienes hi ha a l'esquema lògic  
* La relació referencial és aquella que conté la clau aliena  
* La relació referida és l'origen de la informació. És la part 1, tal com s'ha explicat a classe.

(omple la taula següent amb les claus alienes que hi hagi al cas pràctic)

Relació referencial|Clau aliena|Relació referida
-|:-:|-
habitatge|id_municipi|municipi 
peronsa|num_cadastre|habitatge
propietari|dni|persona 
propietari|num_cadastre|habitatge 
persona|cap_familia|persona

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 1_habitatges.sql](./1_habitagtes.sql)