# 1. El Cinèfil

`Un cinèfil aficionat a la informàtica vol crear una Base de Dades que reculli informació diversa sobre el món cinematogràfic, des dels orígens del cinema fins a avui mateix, amb el contingut que es descriu a continuació.
Lògicament, vol tenir classificades moltes pel.lícules, que vindran identificades per un codi. També vol de cadascuna el nom, l’any de l’estrena, el pressupost, el director, etc. A més, de cada pel.lícula vol conéixer també quins actors van intervenir, així com el paper que hi representàven (actor principal, secundari, etc.) i el possible premi que va rebre per la seva interpretació. Les pel.lícules són d’un tema determinat. Es ben sabut que hi ha actors especialitzats en un tema, encara que un actor és capaç d’interpretar varis temes amb diferent “habilitat”. Com que el nostre cinèfil és una mica curiós, vol emmagatzemar també dades personals dels actors, que ha anat recollint al llegir revistes del món artístic. Per exemple, quins actors són en certa manera substitutius d’altres, amb un grau de possible substitució que pot anar de 1 a 10. També quins actors són “incompatibles”, o sigui, que mai han treballat ni treballaran junts amb una mateixa pel.lícula o escena. Els actors estan contractats, en un moment donat per una companyia, però poden canviar si tenen una oferta millor. També poden retornar a una companyia en la que ja hi  havien treballat. Les companyies produeixen pel.lícules, però cap pel.lícula és coproduïda per dues o més companyies. Com que el nostre amic fa molt de turisme, vol saber, per a cada ciutat, quines companyies hi tenen representació i a quina adreça. Evidentment, les companyies solen tenir representació a quasi totes les ciutats importants. Al mateix temps, vol també informació de quines pel.lícules s’estan rodant a cada ciutat i en quin moment, tenint en compte que una pel.lícula es pot rodar a vàries ciutats i també a una mateixa ciutat en diferents fases del seu rodatge.`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas cinefil](https://drive.google.com/file/d/1DxN7Wz4paS17iVmjA3TjWaGGqEWAtPQo/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas cinefil](./9_cinefil.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  companyia(<ins>id_companyia</ins>, nom)  
  actor(<ins>id_actor</ins>, nom)  
  data(<ins>data_inici</ins>)  
  ciutat(<ins>id_ciutat</ins>, ciutat, pais)  
  fase(<ins>inici_fase</ins>)  
  pelicula(<ins>isbn</ins>, titol, any_estrena, pressupost, director, id_companyia)  
  paper(<ins>id_paper</ins>, descripcio)  
  tema(<ins>id_tema</ins>, descripcio, isbn)  
  contracte(<ins>data_inici</ins>, <ins>id_companyia</ins>, <ins>id_actor</ins>, oferta, data_fi)  
  seu(<ins>id_companyia</ins>, <ins>id_ciutat</ins>, adreca)   
  rodatge(<ins>inici_fase</ins>, <ins>id_ciutat</ins>, <ins>isbn</ins>, fi_fase)  
  interpretacio(<ins>id_actor</ins>, <ins>id_paper</ins>, <ins>isbn</ins>, premi)  
  especialitzacio(<ins>id_actor</ins>, <ins>id_tema</ins>, habilitat)  
  substitucio(<ins>id_actor</ins>, <ins>id_actor_subs</ins>, grau)  
  compatibilitat(<ins>id_actor</ins>, <ins>id_actor_comp</ins>, afinitat) 

## 3.2. Diagrama referencial

* El diagrama referencial em serveix per indicar quines claus alienes hi ha a l'esquema lògic  
* La relació referencial és aquella que conté la clau aliena  
* La relació referida és l'origen de la informació. És la part 1, tal com s'ha explicat a classe.

Relació referencial|Clau aliena|Relació referida
-|:-:|-
pelicula|id_companyia|companyia
tema|isbn|pelicula
contracte|data_inici|data
contracte|id_companyia|companyia
contracte|id_actor|actor
seu|id_companyia|companyia
seu|id_ciutat|ciutat
rodatge|inici_fase|fase
rodatge|id_ciutat|ciutat
rodatge|isbn|pelicula
interpretacio|id_actor|actor
interpretacio|id_paper|paper
interpretacio|isbn|pelicula
especialitzacio|id_actor|actor
especialitzacio|id_tema|tema
substitucio|id_actor|actor
substitucio|id_actor_subs|actor
compatibilitat|id_actor|actor
compatibilitat|id_actor_comp|actor

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 9_cinefil.sql](./9_cinefil.sql)