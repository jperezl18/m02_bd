-- Jorge Daniel Pérez López
-- a232508jp

\c template1
DROP DATABASE IF EXISTS cinefil;
CREATE DATABASE cinefil;
\c cinefil

CREATE TABLE companyia (
    id_companyia INT CONSTRAINT companyia_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT companyia_nom_nn NOT NULL
);

CREATE TABLE actor (
    id_actor INT CONSTRAINT actor_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT actor_nom_nn NOT NULL
);

CREATE TABLE data (
    data_inici DATE CONSTRAINT data_data_inici_pk PRIMARY KEY
);

CREATE TABLE ciutat (
    id_ciutat INT CONSTRAINT ciutat_id_pk PRIMARY KEY,
    ciutat VARCHAR(100) CONSTRAINT ciutat_nom_nn NOT NULL,
    pais VARCHAR(100) CONSTRAINT ciutat_pais_nn NOT NULL
);

CREATE TABLE fase (
    inici_fase DATE CONSTRAINT fase_inici_fase_pk PRIMARY KEY
);

CREATE TABLE pelicula (
    isbn VARCHAR(15) CONSTRAINT pelicula_isbn_pk PRIMARY KEY,
    titol VARCHAR(100) CONSTRAINT pelicula_titol_nn NOT NULL,
    any_estrena NUMERIC(4) CONSTRAINT pelicula_any_estrena_nn NOT NULL,
    pressupost INT,
    director VARCHAR(100),
    id_companyia INT,
    CONSTRAINT pelicula_id_companyia_fk FOREIGN KEY (id_companyia) REFERENCES companyia (id_companyia)
);

CREATE TABLE paper (
    id_paper INT CONSTRAINT paper_id_pk PRIMARY KEY,
    descripcio VARCHAR(256) CONSTRAINT paper_descripcio_nn NOT NULL
);

CREATE TABLE tema (
    id_tema INT CONSTRAINT tema_id_pk PRIMARY KEY,
    descripcio VARCHAR(256) CONSTRAINT tema_descripcio_nn NOT NULL,
    isbn VARCHAR(15),
    CONSTRAINT tema_isbn_fk FOREIGN KEY (isbn) REFERENCES pelicula (isbn)
);

CREATE TABLE contracte (
   data_inici DATE,
   id_companyia INT,
   id_actor INT,
   oferta INT CONSTRAINT contracte_oferta_nn NOT NULL,
   data_fi DATE CONSTRAINT contracte_data_fi_nn NOT NULL,
   CONSTRAINT contracte_pk PRIMARY KEY (data_inici,id_companyia,id_actor),
   CONSTRAINT contracte_data_inici_fk FOREIGN KEY (data_inici) REFERENCES data (data_inici),
   CONSTRAINT contracte_id_companyia_fk FOREIGN KEY (id_companyia) REFERENCES companyia (id_companyia),
   CONSTRAINT contracte_id_actor_fk FOREIGN KEY (id_actor) REFERENCES actor (id_actor)
);

CREATE TABLE seu (
    id_companyia INT,
    id_ciutat INT,
    adreca VARCHAR(100),
    CONSTRAINT seu_pk PRIMARY KEY (id_companyia,id_ciutat),
    CONSTRAINT seu_id_companyia_fk FOREIGN KEY (id_companyia) REFERENCES companyia (id_companyia),
    CONSTRAINT seu_id_ciutat_fk FOREIGN KEY (id_ciutat) REFERENCES ciutat (id_ciutat)
);

CREATE TABLE rodatge (
    inici_fase DATE,
    id_ciutat INT,
    isbn VARCHAR(15),
    fi_fase DATE,
    CONSTRAINT rodatge_pk PRIMARY KEY (inici_fase,id_ciutat,isbn),
    CONSTRAINT rodatge_inici_fase_fk FOREIGN KEY (inici_fase) REFERENCES fase (inici_fase),
    CONSTRAINT rodatge_id_ciutat_fk FOREIGN KEY (id_ciutat) REFERENCES ciutat (id_ciutat),
    CONSTRAINT rodatge_isbn_fk FOREIGN KEY (isbn) REFERENCES pelicula (isbn)
);

CREATE TABLE interpretacio (
    id_actor INT,
    id_paper INT,
    isbn VARCHAR(15),
    premi VARCHAR(100),
    CONSTRAINT interpretacio_pk PRIMARY KEY (id_actor,id_paper,isbn),
    CONSTRAINT interpretacio_id_actor_fk FOREIGN KEY (id_actor) REFERENCES actor (id_actor),
    CONSTRAINT interpretacio_id_paper_fk FOREIGN KEY (id_paper) REFERENCES paper (id_paper),
    CONSTRAINT interpretacio_isbn_fk FOREIGN KEY (isbn) REFERENCES pelicula (isbn)
);

CREATE TABLE especialitzacio (
    id_actor INT,
    id_tema INT,
    habilitat NUMERIC(2) CONSTRAINT especialitzacio_habilitat_nn NOT NULL,
    CONSTRAINT especialitzacio_pk PRIMARY KEY (id_actor,id_tema),
    CONSTRAINT especialitzacio_id_actor_fk FOREIGN KEY (id_actor) REFERENCES actor (id_actor),
    CONSTRAINT especialitzacio_id_tema_fk FOREIGN KEY (id_tema) REFERENCES tema (id_tema)
);

CREATE TABLE substitucio (
    id_actor INT,
    id_actor_subs INT,
    grau NUMERIC(2) CONSTRAINT substitucio_grau_nn NOT NULL,
    CONSTRAINT substitucio_pk PRIMARY KEY (id_actor,id_actor_subs),
    CONSTRAINT substitucio_id_actor_fk FOREIGN KEY (id_actor) REFERENCES actor (id_actor),
    CONSTRAINT substitucio_id_actor_subs_fk FOREIGN KEY (id_actor_subs) REFERENCES actor (id_actor)
);

CREATE TABLE compatibilitat (
    id_actor INT,
    id_actor_comp INT,
    afinitat NUMERIC(2) CONSTRAINT compatibilitat_afinitat_nn NOT NULL,
    CONSTRAINT compatibilitat_pk PRIMARY KEY (id_actor,id_actor_comp),
    CONSTRAINT compatibilitat_id_actor_fk FOREIGN KEY (id_actor) REFERENCES actor (id_actor),
    CONSTRAINT substitucio_id_actor_comp_fk FOREIGN KEY (id_actor_comp) REFERENCES actor (id_actor)
);
