# 1. Gabinet d'advocats

`Es vol dissenyar una base de dades relacional per a emmagatzemar informació
sobre els assumptes que porta un gabinet d'advocats. Cada assumpte té un número d'expedient que l'identifica i correspon a un sol client. De l'assumpte s'ha d'emmagatzemar la data d'inici, data d'arxiu (finalització), el seu estat (en tràmit, arxivat, etc), així com les dades personals del client al qual pertany (DNI, nom, adreça, telèfon). Alguns assumptes són portats per un o diversos procuradors i viceversa, dels quals ens interessa també les dades personals (DNI, nom, adreça, telèfon).`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas advocats](https://drive.google.com/file/d/1a_2YEzmD7TE8pnl3STSXTFyyzBdmYi9a/view?usp=share_link)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas advocats](./2_advocats.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  Client(<ins>DNI</ins>, Nom, Cognoms, Adreça, Telefon)  
  Assumpte(<ins>num_expedient</ins>, data_inici, data_arxiu, estat, DNI)  
  Advocats(<ins>DNI</ins>, Nom, Cognoms, Adreça, Telefon)  
  Advocats per assumpte(<ins>DNI</ins>, <ins>num_expedient</ins>)

## 3.2. Diagrama referencial
Relació referencial|Clau aliena|Relació referida
-|:-:|-
Assumpte|DNI|Client
Advocats per assumpte|DNI|Advocats
Advocats per assumpte|num_expedient|Assumpte

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 2_gabinet.sql](./2_gabinet.sql)