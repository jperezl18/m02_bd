-- Jorge Daniel Pérez López
-- a232508jp

\c template1
DROP DATABASE IF EXISTS zoos;
CREATE DATABASE zoos;
\c zoos

CREATE TABLE especie (
    id_especie INT CONSTRAINT especie_id_especie_pk PRIMARY KEY,
    nom_vulgar VARCHAR(100) CONSTRAINT especie_nom_vulgar_nn NOT NULL,
    nom_cientific VARCHAR(100) CONSTRAINT especie_nom_cientific_nn NOT NULL,
    familia VARCHAR(100),
    conservacio VARCHAR(100)
);

CREATE TABLE zoo (
    id_zoo INT CONSTRAINT zoo_id_zoo_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT zoo_nom_nn NOT NULL,
    ciutat VARCHAR(100) CONSTRAINT zoo_ciutat_nn NOT NULL,
    pais VARCHAR(100) CONSTRAINT zoo_pais_nn NOT NULL,
    mida_m2 SMALLINT,
    pressupost_anual INT CONSTRAINT zoo_pressupost_anual_nn NOT NULL
);

CREATE TABLE animal (
    id_zoo INT,
    id_animal INT,
    sexe VARCHAR(1) CONSTRAINT animal_sexe_ck CHECK (sexe = 'M' OR sexe = 'F'),
    any_naixement SMALLINT,
    pais_origen VARCHAR(50),
    continent VARCHAR(100),
    id_especie INT,
    CONSTRAINT animal_pk PRIMARY KEY (id_zoo,id_animal),
    CONSTRAINT animal_id_zoo_fk FOREIGN KEY (id_zoo) REFERENCES zoo (id_zoo),
    CONSTRAINT animal_id_especie_fk FOREIGN KEY (id_especie) REFERENCES especie (id_especie)
);

INSERT INTO especie VALUES
(1, 'Silur', 'Silurus Glanis', 'Siluridae', 'Risc mínim'),
(2, 'Rinoceront blanc', 'Ceratotherium simum', 'Rhinocerotidae', 'Gairebé amenaçada'),
(3, 'Linx ibèric', 'Lynx pardinus', 'Felidae', 'En perill'),
(4, 'Bec desclop', 'Balaeniceps rex', 'Balaenicipitidae', 'Vulnerable'),
(5, 'Elefant asiàtic', 'Elephas maximus', 'Elephantidae', 'En perill'),
(6, 'Cobra Reial', 'Ophiophagus hannah', 'Elapidae', 'Vulnerable'),
(7, 'Caputxí de cara blanca', 'Cebus capucinus', 'Cebidae', 'Vulnerable'),
(8, 'Pingüí emperador', 'Aptenodytes forsteri', 'Spheniscidae', 'Gairebé amenaçada'),
(9, 'Toixó', 'Meles meles', 'Mustelidae', 'Risc mínim'),
(10, 'Tortuga aligator', 'Macrochelys temminckii', 'Chelydridae', 'Vulnerable');

INSERT INTO zoo VALUES
(1, 'Zoo Barcelona', 'Barcelona', 'Espanya', 0.12, 2711110),
(2, 'Henry Doorly Zoo', 'Omaha', 'Estats Units', 0.52, 36000000);

INSERT INTO animal VALUES
(2, 1, 'M', 2004, 'Ucraïna', 'Europa', 1),
(1, 2, 'F', 2003, 'Hongria', 'Europa', 1),
(2, 3, 'F', 1995, 'Botswana', 'Àfrica', 2),
(2, 4, 'M', 2012, 'Espanya', 'Europa', 3),
(2, 5, 'M', 2019, 'Sudan del Sud', 'Àfrica', 4),
(2, 6, 'F', 2010, 'Sri Lanka', 'Àsia', 5),
(1, 7, 'M', 2015, 'Malàisia', 'Àsia', 6),
(2, 8, 'F', 2018, 'Colòmbia', 'Amèrica del Sud', 7),
(2, 9, 'F', 2017, null, 'Antàrtida', 8),
(1, 10, 'F', 2015, null, 'Antàrtida', 8),
(1, 11, 'F', 2014, 'Polònia', 'Europa', 9),
(2 ,12, 'M', 2010, 'Eslovàquia', 'Europa', 9),
(2, 13, 'M', null, null, null, 10);
