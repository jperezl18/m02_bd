# 1. Publicitat (versió curta)

`Si voleu implementar una base de dades que almacene informació sobre la publicitat actualment emesa en els mitjans principals de comunicació. Hi ha tres tipus de suports publicitaris: televisió, ràdio i premsa escrita. De los anuncios se conoce el lema que utilitzen. Si l'anunci és televisiu o és una canya radiofónica, si voleu conèixer alguns minuts de durada, el nombre de les cadenes de televisió o emissores de ràdio respectivament que emeten i quantes vegades al dia s'emeten en cada un dels mitjans. Si l'anunci està imprès s'emmagatzemarà el nom i la tirada de les publicacions. De los anuncios impresos podran tener imagen o no . Un anunci pertany a una campanya publicitària que pot incloure altres anuncis. Cada campanya publicitària té un tema (venta d'un producte, promoció del turisme en una zona determinada, ajuda a determinats països, prevenció de malalties,...) i un pressupost total per a tots els anuncis que inclouen els mateixos. Dicha campanya publicitària la contracta un anunciant del que coneixem el seu nom i si és institució o empresa. Finalmente se quiere contemplar cuáles de los medios audiovisuales (es decir cadenas de televisión y emisoras de ràdio) considerades abans son a les seves empreses que s'anunciïn.
Nota: establertes les claus primàries que consideren oportunitats.`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas publicitat(curta)](https://drive.google.com/file/d/1HY1oiBdxpu7Oo_yKYz3okA63xPotQYvP/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas publicitat(curta)](./7_publicitat_curta.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  anunciant(<ins>id_anunciant</ins>, nom, institucio_empresa)  
  campanya_publicitaria(<ins>id_campanya</ins>, tema, pressupost_total)  
  campanya per anunciant(<ins>id_anunciant</ins>, <ins>id_campanya</ins>)  
  anunci(<ins>id_anunci</ins>, lema, id_campanya)  
  televisio(<ins>id_anunci</ins>, lema, durada, mun_cadenes, emissions_diaries)  
  radio(<ins>id_anunci</ins>, lema, durada, num_emissiores, emissions_diaries)  
  premsa_escrita(<ins>id_anunci</ins>, lema, nom, tirada, imatge)
  

## 3.2. Diagrama referencial
Relació referencial|Clau aliena|Relació referida
-|:-:|-
campanya per anunciant|id_anunciant|anunciant
campanya per anunciant|id_campanya|campanya_publicitaria
anunci|id_campanya|campanya_publicitaria

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)