-- Jorge Daniel Pérez López
-- a232508jp

\c template1
DROP DATABASE IF EXISTS gabinet;
CREATE DATABASE gabinet;
\c gabinet

CREATE TABLE client (
    dni VARCHAR(9) CONSTRAINT client_dni_pk PRIMARY KEY, 
    nom VARCHAR(100) CONSTRAINT client_nom_nn NOT NULL, 
    cognoms VARCHAR(100) CONSTRAINT client_cognoms_nn NOT NULL, 
    adreca VARCHAR(100), 
    telefon VARCHAR(20) CONSTRAINT client_telefon_nn NOT NULL
    );

CREATE TABLE assumpte (
    num_expedient VARCHAR(50) CONSTRAINT assumpte_num_expedient_pk PRIMARY KEY,
    data_inici DATE CONSTRAINT assumpte_data_inici_nn NOT NULL,
    data_arxiu DATE,
    estat VARCHAR(1) CONSTRAINT assumpte_estat_ck CHECK (estat = 'T' OR estat = 'O'),
    dni VARCHAR(9),
    CONSTRAINT assumpte_dni_fk FOREIGN KEY (dni) REFERENCES client (dni)
    );

CREATE TABLE procurador (
    dni VARCHAR(9) CONSTRAINT procurador_dni_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT procurador_nom_uk UNIQUE, 
    cognoms VARCHAR(100) CONSTRAINT procurador_cognoms_nn NOT NULL, 
    adreca VARCHAR(100), 
    telefon VARCHAR(20) CONSTRAINT procurador_telefon_nn NOT NULL
    );

CREATE TABLE procuradors_per_assumpte (
    dni VARCHAR(9),
    num_expedient VARCHAR(50),
    CONSTRAINT procuradors_per_assumpte_pk PRIMARY KEY (dni,num_expedient),
    CONSTRAINT procuradors_per_assumpte_dni_fk FOREIGN KEY (dni) REFERENCES procurador (dni),
    CONSTRAINT procuradors_per_assumpte_num_expedient_fk FOREIGN KEY (num_expedient) REFERENCES assumpte (num_expedient)
    );

INSERT INTO client VALUES
('O4031927', 'Jorge Daniel', 'Pérez López', 'Carrer del Cedre, 11, 5é 1a, Santa Coloma, Andorra la Vella, AD500, Andorra', '+376682744'),
('12345678Z', 'Fernando "El Nano"', 'Alonso Díaz', null, 333333333),
('87654321H', 'Pedro', 'Sánchez Pérez-Castejón', 'Av. Puerta de Hierro, s/n, 28071 Madrid', 913353535),
('14141414J', 'Joselu', 'Sanmartín Malo', 'Av. del Baix Llobregat, 100, 08940 Cornellà de Llobregat, Barcelona', 932927700);

INSERT INTO procurador VALUES
('56743892E', 'Eduard', 'Canet', null, 123456789),
('71298312W', 'Marc', 'Jordi Casanoves', '4 Carrer del Doctor Pi i Molist, Barcelona, Cataluña', 693719141),
('O4124881', 'Joan-Enric', 'Vives i Sicília', 'Plaça del Deganat, 16, 25700 La Seu dUrgell, Lleida', '+376912833'),
('72389126T', 'Carlos', 'Slim', 'Carrer de lHospital, 34, 08001 Barcelona', 933425035);

INSERT INTO assumpte VALUES
(1, '1999-12-04', '2000-01-07', 'T', 'O4031927'),
(2, '2016-02-29', '2019-11-18', 'T', 'O4031927'),
(3, '2023-06-19', null, 'O', '14141414J'),
(4, '2009-10-05', '2015-03-25', 'T', '12345678Z'),
(5, '2018-06-02', null, 'O', '87654321H');

INSERT INTO procuradors_per_assumpte VALUES
('56743892E', 1),
('56743892E', 2),
('71298312W', 2),
('71298312W', 3),
('O4124881', 4),
('72389126T', 5);