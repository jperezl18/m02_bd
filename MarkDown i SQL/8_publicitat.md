# 1. Anuncis publicitat  (versió llarga)

`Es proposa dissenyar una BD per millorar el control dels "spots" (anuncis) per a la televisió, i de tot el seu entorn, com són els canals de TV, franges horàries, agències de publicitat, tipus de productes que s'anuncien etc.
Al país hi ha diversos ens televisius. Es vol reflectir que uns són de titularitat pública, per exemple CCMA (Corporació Catalana de Mitjans Audiovisuals) i RTVE (Ràdio-Televisió Espanyola) i altres són de titularitat privada com Atresmedia. Aquests ens televisius disposen d'un o més canals d'emissió. Per exemple, CCMA disposa de TV3, El 33, Canal Super3, etc. RTVE disposa de La 1, La 2, 24 hores, Tododeporte, etc. Atresmedia disposa de La Sexta, Antena 3, etc. Cada canal s'identificarà sempre per un codi, tindrà un nom i una descripció.
Tots els "espots" suposarem que s'identifiquen per un codi assignat per una suposada "Oficina de Mitjans de Comunicació". Ha de considerar l'existència de "spots" equivalents, els quals podrien ser aquells que tenen les mateixes imatges però tenen diferent idioma.
Cada "espot" fa referència a un (el normal) o més (excepcionalment) tipus de productes (pensar en rentadores i detergents), i es vol tenir constància d'aquestes referències. Fins i tot s'ha de pensar que hi ha tipificats certs tipus de productes, independentment de si hi ha o no, en aquest moment, espots que facin referència a ells. Els "spots" són sempre propietat d'una única firma comercial, que és la que els paga, fins i tot en el cas d'anunciar més d'un tipus de producte.
Els "spots" són filmats, normalment, per una agència de publicitat encara que, en alguns casos, és la mateixa firma comercial que, amb mitjans propis, els produeix sense intervenció de cap agència publicitària.
Tal com s'ha indicat a l'principi, interessa també conèixer l'emissió dels "spots" en els diversos canals televisius. A efectes d'audiència, les 24 hores del dia no s'entenen com a tals hores, sinó com a "franges horàries" (com poden ser: matí, migdia, tarda, nit, ...) perfectament diferenciades. És més, el preu vigent (únic que interessa) de l'emissió en cada canal, depèn de la seva franja horària. Per simplificar el cas, farem la hipòtesi que el preu no depèn del dia de la setmana. Es vol tenir constància de l'nombre de vegades que cada "espot" s'ha emès en els diferents canals, la seva data d'emissió i la seva corresponent franja horària.
Finalment, hi ha un aspecte legal molt important a considerar. Hi ha alguns tipus de productes (realment molt pocs) que no estan permesos anunciar en certes franges horàries. L'incompliment serà penalitzat amb multes de gravetat qualificada d'1 a 3. Interessarà tenir constància de les prohibicions legals anteriors.`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas publicitat (llarga)](https://drive.google.com/file/d/1JAPKojjkpCIN9XoBOuI6WR1s1yGYQ8ZU/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas publicitat_llarga](./8_publicitat.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  ens(<ins>id_ens</ins>, nom, tipus)  
  canal(<ins>id_canal</ins>, nom, descripcio, id_ens)  
  franja(<ins>id_franja</ins>, franja, hora_inici, hora_fi)  
  empresa(<ins>cif</ins>, nom, adreca, telefon)  
  firma_comercial(<ins>cif_firma</ins>)  
  agencia_comercial(<inx>cif_agencia</ins>)  
  spot(<ins>id_spot</ins>, idioma, cif, cif_firma)  
  tipus_producte(<ins>id_tipus_producte</ins>, tipus_producte)  
  emissio(<ins>id_canal</ins>, <ins>id_spot</ins>, <ins>id_franja</ins>, num_emissions, preu, data)  
  spot_equivalent(<ins>id_spot</ins>, <ins>id_spot_equivalent</ins>)  
  tipus_producte_x_spot(<ins>id_spot</ins>, <ins>id_tipus_producte</ins>)  
  tipus_producte_prohibit(<ins>id_franja</ins>, <ins>id_tipus_producte</ins>)


  

## 3.2. Diagrama referencial

* El diagrama referencial em serveix per indicar quines claus alienes hi ha a l'esquema lògic  
* La relació referencial és aquella que conté la clau aliena  
* La relació referida és l'origen de la informació. És la part 1, tal com s'ha explicat a classe.

Relació referencial|Clau aliena|Relació referida
-|:-:|-
canal|id_ens|ens
spot|cif|empresa
spot|cif_firma|firma_comercial
emissio|id_canal|canal
emissio|id_spot|spot
emissio|id_franja|franja
spot_equivalent|id_spot_equivalent|spot
tipus_producte_x_spot|id_spot|spot
tipus_producte_x_spot|id_tipus_producte|tipus_producte
tipus_producte_prohibit|id_franja|franja
tipus_producte_prohibit|id_tipus_producte|tipus_producte


# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 8_publicitat.sql](./8_publicitat.sql)