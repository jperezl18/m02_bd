# 1. Carreteres

`Dissenyar una base de dades que contingui informació relativa a totes les carreteres d'un determinat país. Es demana realitzar el disseny en el model E/R, sabent que:
En aquest paıs les carreteres es troben dividides en trams.
Un tram sempre pertany a una única carretera i no pot canviar de carretera.
Un tram pot passar per diversos termes municipals, sent una dada d'interès al km. del tram pel qual entra en dit terme municipal i al km. pel qual surt.
Hi ha una sèrie d'àrees en les que s'agrupen els trams, cada un dels quals no pot pertànyer a més d'una àrea.
Establiu els atributs que considereu oportuns i d'ells, trieu indentificador per a cada entitat.`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas carreteres](https://drive.google.com/file/d/1a_deDVmEqTUcTPKlA3sb1Q2B2EuEomXP/view?usp=drive_link)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas carreteres](./4_carreteres.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  carretera(<ins>idCarretera</ins>, nom, longitud)  
  tram(<ins>idTram</ins>, longitud, idCarretera, idArea)
  area(<ins>idArea</ins>, tamany)  
  terme_municipal(<ins>idMunicipi</ins>, nom, kmEntrada, kmSortida)  
  tram per municipi(<ins>idTram</ins>, <ins>idMunicipi</ins>)  
  
  

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
tram|idCarretera|carretera
tram|idArea|area

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 4_carreteres.sql](./4_carreteres.sql)