\c template1
DROP DATABASE IF EXISTS carreteres;
CREATE DATABASE carreteres;
\c carreteres

CREATE TABLE carretera (
    id_carretera INT CONSTRAINT carretera_id_carretera_pk PRIMARY KEY,
    nom VARCHAR(100),
    longitud SMALLINT CONSTRAINT carretera_longitud_nn NOT NULL
);

CREATE TABLE area (
    id_area INT CONSTRAINT area_id_area_pk PRIMARY KEY,
    tamany INT CONSTRAINT area_tamany_nn NOT NULL
);

CREATE TABLE tram (
    id_tram INT CONSTRAINT tram_id_tram_pk PRIMARY KEY,
    longitud SMALLINT CONSTRAINT tram_longitud_nn NOT NULL,
    id_carretera INT,
    id_area INT,
    CONSTRAINT tram_id_carretera_fk FOREIGN KEY (id_carretera) REFERENCES carretera (id_carretera),
    CONSTRAINT tram_id_area_fk FOREIGN KEY (id_area) REFERENCES area (id_area)
);

CREATE TABLE terme_municipal (
    id_municipi INT CONSTRAINT terme_municipal_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT terme_municipal_nom_nn NOT NULL,
    km_entrada SMALLINT CONSTRAINT terme_municipal_km_entrada_nn NOT NULL,
    km_sortida SMALLINT CONSTRAINT terme_municipal_km_sortida_nn NOT NULL
);

CREATE TABLE trams_per_municipi (
    id_tram INT,
    id_municipi INT,
    CONSTRAINT trams_per_municipi_pk PRIMARY KEY (id_tram,id_municipi),
    CONSTRAINT trams_per_municipi_id_tram_fk FOREIGN KEY (id_tram) REFERENCES tram (id_tram),
    CONSTRAINT trams_per_municipi_id_municipi_fk FOREIGN KEY (id_municipi) REFERENCES terme_municipal (id_municipi)
);