# 1. Zoos

`Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen. De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual. Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi. De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció. A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.`

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas zoos](https://drive.google.com/file/d/1Qlav9W-RgRUfYteAVHCHEn5eaRfbM4Qe/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas zoos](./3_zoos.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  Animal(<ins>id_zoo</ins>, <ins>id_animal</ins>, sexe, any de naixement, pais d'origen, continent, id_especie)  
  Espècie(<ins>id_especie</ins>, nom vulgar, nom cientific, familia, conservacio)  
  Zoo(<ins>id_zoo</ins>, nom, ciutat, pais, mida, pressupost anual)
  
  
## 3.2. Diagrama referencial
Relació referencial|Clau aliena|Relació referida
-|:-:|-
Animal|id_especie|Espècie
Zoo|id_animal|Animal

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script 3_zoos.sql](./3_zoos.sql)