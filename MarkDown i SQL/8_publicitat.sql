-- Jorge Daniel Pérez López
-- a232508jp

\c template1
DROP DATABASE IF EXISTS publicitat_llarga;
CREATE DATABASE publicitat_llarga;
\c publicitat_llarga

CREATE TABLE ens (
    id_ens INT CONSTRAINT ens_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT ens_nom_nn NOT NULL,
    tipus VARCHAR(6) CONSTRAINT ens_tipus_ck CHECK (tipus = 'Públic' OR tipus = 'Privat')
);

CREATE TABLE canal (
    id_canal INT CONSTRAINT canal_id_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT canal_nom_nn NOT NULL,
    descripcio VARCHAR(256),
    id_ens INT,
    CONSTRAINT canal_id_ens_fk FOREIGN KEY (id_ens) REFERENCES ens (id_ens)
);

CREATE TABLE franja (
    id_franja SMALLINT CONSTRAINT franja_id_pk PRIMARY KEY,
    franja VARCHAR(100) CONSTRAINT franja_franja_nn NOT NULL,
    hora_inici VARCHAR(5) CONSTRAINT franja_hora_inici_nn NOT NULL,
    hora_fi VARCHAR(5) CONSTRAINT franja_hora_fi_nn NOT NULL
);

CREATE TABLE empresa (
    cif VARCHAR(9) CONSTRAINT empresa_cif_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT empresa_nom_nn NOT NULL,
    adreca VARCHAR(100),
    telefon VARCHAR(15) CONSTRAINT empresa_telefon_nn NOT NULL
);

CREATE TABLE firma_comercial (
    cif_firma VARCHAR(9) CONSTRAINT firma_comercial_cif_pk PRIMARY KEY,
    CONSTRAINT firma_comercial_cif_fk FOREIGN KEY (cif_firma) REFERENCES empresa (cif)
);

CREATE TABLE agencia_comercial (
    cif_agencia VARCHAR(9) CONSTRAINT agencia_comercial_cif_pk PRIMARY KEY,
    CONSTRAINT agencia_comercial_cif_fk FOREIGN KEY (cif_agencia) REFERENCES empresa (cif)
);

CREATE TABLE spot (
    id_spot INT CONSTRAINT spot_id_pk PRIMARY KEY,
    idioma VARCHAR(100) CONSTRAINT spot_idioma_nn NOT NULL,
    cif VARCHAR(9),
    cif_firma VARCHAR(9),
    CONSTRAINT spot_cif_fk FOREIGN KEY (cif) REFERENCES empresa (cif),
    CONSTRAINT spot_cif_firma_fk FOREIGN KEY (cif_firma) REFERENCES firma_comercial (cif_firma)
);

CREATE TABLE tipus_producte (
    id_tipus_producte INT CONSTRAINT tipus_producte_id_pk PRIMARY KEY,
    tipus_producte VARCHAR(100) CONSTRAINT tipus_producte_nn NOT NULL
);

CREATE TABLE emissio (
    id_canal INT,
    id_spot INT,
    id_franja SMALLINT,
    num_emissions SMALLINT CONSTRAINT emissio_num_emissions_nn NOT NULL,
    preu INT CONSTRAINT emissio_preu_nn NOT NULL,
    data DATE CONSTRAINT emissio_data_nn NOT NULL,
    CONSTRAINT emissio_pk PRIMARY KEY (id_canal,id_spot,id_franja),
    CONSTRAINT emissio_id_canal_fk FOREIGN KEY (id_canal) REFERENCES canal (id_canal),
    CONSTRAINT emissio_id_spot_fk FOREIGN KEY (id_spot) REFERENCES spot (id_spot),
    CONSTRAINT emissio_id_franja_fk FOREIGN KEY (id_franja) REFERENCES franja (id_franja)
);

CREATE TABLE spot_equivalent (
    id_spot INT,
    id_spot_equivalent INT,
    CONSTRAINT spot_equivalent_pk PRIMARY KEY (id_spot,id_spot_equivalent),
    CONSTRAINT spot_equivalent_id_spot_fk FOREIGN KEY (id_spot) REFERENCES spot (id_spot),
    CONSTRAINT spot_equivalent_id_spot_equivalent_fk FOREIGN KEY (id_spot_equivalent) REFERENCES spot (id_spot)
);

CREATE TABLE tipus_producte_x_spot (
    id_spot INT,
    id_tipus_producte INT,
    CONSTRAINT tipus_producte_x_spot_pk PRIMARY KEY (id_spot,id_tipus_producte),
    CONSTRAINT tipus_producte_x_spot_id_spot_fk FOREIGN KEY (id_spot) REFERENCES spot (id_spot),
    CONSTRAINT tipus_producte_x_spot_id_tipus_producte_fk FOREIGN KEY (id_tipus_producte) REFERENCES tipus_producte (id_tipus_producte)
);

CREATE TABLE tipus_producte_prohibit (
    id_franja SMALLINT,
    id_tipus_producte INT,
    CONSTRAINT tipus_producte_prohibit_pk PRIMARY KEY (id_franja,id_tipus_producte),
    CONSTRAINT tipus_producte_prohibit_id_franja_fk FOREIGN KEY (id_franja) REFERENCES franja (id_franja),
    CONSTRAINT tipus_producte_prohibit_id_tipus_producte_fk FOREIGN KEY (id_tipus_producte) REFERENCES tipus_producte (id_tipus_producte)
);

INSERT INTO ens VALUES
(1, 'RTVE', 'Públic'),
(2, 'Atresmedia', 'Privat');

INSERT INTO canal VALUES
(1, 'La 1', 'Programación generalista para todos los públicos, centrada en la información y el entretenimiento, que incluye programas informativos, series de ficción, concursos o cine, entre otros.', 1),
(2, 'La 2', 'Su programación se basa en espacios culturales, de servicio público y con una vocación minoritaria respecto a La 1, su canal hermano.', 1),
(3, 'Antena 3', 'Antena 3 és un canal de televisió privat espanyol, dàmbit nacional, que emet a la TDT. Està operada per Antena 3 de Televisión, que a la vegada forma part de Atresmedia', 2),
(4, 'La Sexta', 'La programació és generalista, de tendència progressista, encara que amb especial presència de programes dhumor i entreteniment, sèries nord-americanes, tertúlies danàlisi social i polític, i retransmissions esportives.', 2);

INSERT INTO franja VALUES
(1, 'Matí', '07:00', '12:59'),
(2, 'Migdia', '13:00', '14:59'),
(3, 'Tarda', '15:00', '19:59'),
(4, 'Nit', '20:00', '06:59');

INSERT INTO empresa VALUES
('A08005449', 'Nestle España SAU', 'Carrer de Clara Campoamor, 2, 08950 Esplugues de Llobregat, Barcelona', 934805100),
('B77777777', 'Publicidades Manolo', null, '+376682744'),
('B83497396', 'KIA Motors Iberia', 'Calle Anabel Segura, 16. Edificio Vega Norte, 2, 28108 Alcobendas (Madrid)', 915796466),
('C22222222', 'Marquetings Raval, S.A', null, '+355682057012'),
('R69696969', 'Microsistemes Raval', 'Carrer de lHospital, 23, 08001 Barcelona', 934127802);

INSERT INTO firma_comercial VALUES
('B77777777'),
('C22222222'),
('A08005449'),
('B83497396'),
('R69696969');

INSERT INTO agencia_comercial VALUES
('A08005449'),
('B83497396'),
('R69696969');

INSERT INTO spot VALUES
(1, 'Català', 'A08005449', 'A08005449'),
(2, 'Castellà', 'B83497396', 'B83497396'),
(3, 'Català', 'R69696969', 'C22222222'),
(4, 'Castellà', 'A08005449', 'A08005449');

INSERT INTO tipus_producte VALUES
(1, 'Cereals'),
(2, 'Automòbils'),
(3, 'Microprocessadors'),
(4, 'Tarjetes gràfiques');

INSERT INTO emissio (id_canal, id_spot, id_franja, num_emissions, preu, data) VALUES
(1, 1, 1, 25, 150000, '18-11-2003'),
(2, 1, 2, 19, 119000, '13-02-1995'),
(3, 2, 3, 4, 25000, '30-06-2015'),
(4, 3, 4, 6, 1237, '23-04-2017');

INSERT INTO spot_equivalent VALUES
(1, 4);

INSERT INTO tipus_producte_x_spot VALUES
(1, 1),
(2, 2),
(3, 3),
(3, 4),
(4, 1);

INSERT INTO tipus_producte_prohibit VALUES
(2, 1),
(3, 1),
(4, 2),
(1, 2),
(1, 3),
(2, 3),
(4, 3),
(2, 4),
(3, 4);