-- Jorge Daniel Pérez López
-- a232508jp

\c template1
DROP DATABASE IF EXISTS habitatges;
CREATE DATABASE habitatges;
\c habitatges

CREATE TABLE municipi (
    id_municipi INT CONSTRAINT municipi_id_municipi_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT municipi_nom_nn NOT NULL,
    habitants BIGINT CONSTRAINT municipi_habitants_nn NOT NULL,
    extensio_km2 INT CONSTRAINT municipi_extensio_nn NOT NULL
);

CREATE TABLE habitatge (
    num_cadastre INT CONSTRAINT habitatge_num_cadastre_pk PRIMARY KEY,
    carrer VARCHAR(100) CONSTRAINT habitatge_carrer_nn NOT NULL,
    num NUMERIC(3) CONSTRAINT habitatge_num_nn NOT NULL,
    pis NUMERIC(2),
    porta VARCHAR(4),
    escala VARCHAR(2),
    cp VARCHAR(5) CONSTRAINT habitatge_cp_nn NOT NULL,
    tamany_m2 SMALLINT CONSTRAINT habitatge_m2_nn NOT NULL,
    id_municipi INT,
    CONSTRAINT habitatge_id_municipi_fk FOREIGN KEY (id_municipi) REFERENCES municipi (id_municipi) 
);

CREATE TABLE persona (
    dni VARCHAR(9) CONSTRAINT persona_dni_pk PRIMARY KEY,
    nom VARCHAR(100) CONSTRAINT persona_nom_nn NOT NULL,
    cognoms VARCHAR(100) CONSTRAINT persona_cognoms_nn NOT NULL,
    num_cadastre INT,
    cap_familia VARCHAR(9),
    CONSTRAINT persona_num_cadastre_fk FOREIGN KEY (num_cadastre) REFERENCES habitatge (num_cadastre),
    CONSTRAINT persona_cap_familia_fk FOREIGN KEY (cap_familia) REFERENCES persona (dni)
);

CREATE TABLE propietat (
    dni VARCHAR(9),
    num_cadastre INT,
    CONSTRAINT propietat_pk PRIMARY KEY (dni,num_cadastre),
    CONSTRAINT propietat_dni_fk FOREIGN KEY (dni) REFERENCES persona (dni),
    CONSTRAINT propietat_num_cadestre_fk FOREIGN KEY (num_cadastre) REFERENCES habitatge (num_cadastre)
);

INSERT INTO municipi
VALUES (1, 'Andorra la Vella', 23324, 12), 
(2, 'San Sebastián', 186665, 60.89);

INSERT INTO habitatge
VALUES (1, 'Carrer del Cedre', 11, 5, 1, null, 'AD500', 65, 1),
(2, 'Calle de María Dolores Aguirre', 20, 5, 'B', null, 20012, 23, 2);

INSERT INTO persona
VALUES ('12345678Z', 'Jorge', 'Pérez Dorado', 1, null),
('O4031927', 'Jorge Daniel', 'Pérez López', 1, '12345678Z'),
('87654321H', 'Milagros', 'López López', 2, null),
('33280469F', 'Nil', 'Espinosa', 2, '87654321H');

INSERT INTO propietat
VALUES ('12345678Z', 1),
('12345678Z', 2),
('87654321H', 2);